FROM easye/golang:20160713c

ENV     id              go-libp2p

LABEL   id              "${id}"
LABEL   cloudfleet_id   bitbucket.org:easye/${id}
LABEL   "VERSION"       "0.0.1"

ENV dest_root           /usr/local
ENV git_uri      github.com/ipfs/${id}/...

RUN export DEBIAN_FRONTEND='noninteractive' && \
    apt-get update  && \
    apt-get install -y screen git wget

RUN mkdir -p "$HOME/go"
RUN echo "export GOROOT=/usr/local/go" >> ~/.bashrc
RUN echo "PATH=$PATH:$GOROOT/bin" >> ~/.bashrc
RUN echo "export GOPATH=$HOME/go" >> ~/.bashrc

ENV   zeronet_root      "/opt/io/zeronet"
      COPY             .      "${zeronet_root}"
WORKDIR          ${zeronet_root}

RUN ["./go-build.bash", "github.com/ipfs/go-libp2p/..."]

CMD bash




    
